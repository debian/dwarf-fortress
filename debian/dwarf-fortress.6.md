% DWARF-FORTRESS(6) | Games
% Sven Bartscher
% 2017-11-24

# NAME

dwarf-fortress - Slaves to Armok: God of Blood Chapter II: Dwarf
Fortress

# SYNOPSIS

**dwarf-fortress** [**-X**] [**-gen** *id_number* *seed* *worldgen_parameter*...]

# DESCRIPTION

**dwarf-fortress** is a single-player fantasy game. You can control a
dwarven outpost or an adventurer in a randomly generated, persistent
world.

# OPTIONS

## -gen *id_number* *seed* *worldgen_parameter*...
Directly start the world generation with the given parameters. 

# NOTES

Dwarf Fortress as published on http://bay12games.com/dwarves/ does not
support installing data and library files to different locations. This
version of Dwarf Fortress works around this by creating a temporary
directory in /tmp and symlinking all relevant game data and binaries
there. This makes it possible to supply additional data files in
*/usr/local/share/dwarf-fortress/* and
*\$XDG_DATA_HOME/dwarf-fortress/*. Modifications made by Dwarf
Fortress will be retained in the latter directory.
